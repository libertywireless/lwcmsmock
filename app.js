const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

const server = app.listen(port);

server.timeout = 1000 * 60 * 10; // 10 minutes

// Use middleware to set the default Content-Type
app.use(function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next();
});

app.get('/telco/dashboard-base-summary', (req, res) => {
var fs = require("fs");
var text = fs.readFileSync("./telco/dashboard-base-summary.json", "utf-8");
res.send(text);
})

app.get('/telco/dashboard-base-details', (req, res) => {
var fs = require("fs");
var text = fs.readFileSync("./telco/dashboard-base-details.json", "utf-8");
res.send(text);
})

app.get('/telco/dashboard', (req, res) => {
    var fs = require("fs");
    var text = fs.readFileSync("./telco/dashboard.json", "utf-8");
    res.send(text);
})

app.get('/api/endpoint1', (req, res) => {
    res.send(JSON.stringify({value: 1}));
})
