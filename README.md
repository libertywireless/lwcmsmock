## API Mock Test Server
### Heroku

[Confluence link](https://libertywireless.atlassian.net/wiki/spaces/CCM/pages/643137548/Mock+Server)

##### Installation
```
$ brew install heroku/brew/heroku
$ heroku login
```

###### Clone
```
$ heroku git:clone -a lwcmsmock
$ cd lwcmsmock
```

###### Adding files
create mock json files eg:`dashboard.json`
Add the following route to `app.js`

```
app.get('/dashboard', (req, res) => {
var fs = require("fs");
var text = fs.readFileSync("./dashboard.json", "utf-8");
res.send(text);
})
```

###### Running locally
```
$ npm install
$ npm start
```

Go to `localhost:PORT/:ENDPOINT`

Sample: [localhost:3000/telco/dashboard](http://localhost:3000/telco/dashboard)

###### Deployment
```
$ git add .
$ git commit -am "feat:dashboard mock"
$ git push heroku master
```

###### Testing
[https://lwcmsmock.herokuapp.com/telco/dashboard](https://lwcmsmock.herokuapp.com/telco/dashboard)
